<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Phone;
use App\Models\Email;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::with('phone','email')->search()->paginate(12);
        return view('contacts.index',compact(['contacts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $phones = Phone::all();
        $emails = Email::all();
        return view('contacts.create',compact(['phones','emails']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image_name')->store('contacts');
        $contact = Contact::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'birthdate' => $request->birthdate,
            'address' => $request->address,
            'image_name' => $image,
        ]);

        $emails = $request->emails;
        foreach($emails as $email) {
            $isPrimary = ($email === $request->selected_email) ? 1 : 0;
            $contact->email()->create(['email' => $email, 'is_primary' => $isPrimary]);
        }

        $phones = $request->telephones;
        foreach($phones as $phone) {
            $isPrimary = ($phone === $request->selected_telephone) ? 1 : 0;
            $contact->phone()->create(['telephone' => $phone, 'is_primary' => $isPrimary]);
        }

        return redirect(route('contacts.index'))
            ->with([
                'status' => 'success',
                'message' => 'Contact Added Successfully!'
            ]
        );
    }

    /**
     * Display the specified resource.s
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        $phones = Phone::all();
        $emails = Email::all();
        return view('contacts.edit', compact(['contact','phones','emails']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $data = $request->all();

        if($request->hasFile('image_name')) {
            $image = $request->file('image_name')->store('contacts');
            $data['image_name'] = $image;
            $contact->deleteImage();
        }

        $emails = $request->emails;
        $contact->email()->update(['is_primary' => '0']);
        foreach ($emails as $emailIndex => $newEmail) {
            $emailExist = $contact->email->get($emailIndex);
            if ($emailExist) {
                $emailExist->email = $newEmail;
                $emailExist->where('email', $request->selected_email)->update(['is_primary' => '1']);
                $emailExist->save();
            } else {
                $contact->email()->create(['email' => $newEmail]);
            }
            $contact->email()->where('email', $request->selected_email)->update(['is_primary' => '1']);
        }

        $phones = $request->telephones;
        $contact->phone()->update(['is_primary' => '0']);
        foreach ($phones as $phoneIndex => $newPhone) {
            $phoneExist = $contact->phone->get($phoneIndex);
            if ($phoneExist) {
                $phoneExist->telephone = $newPhone;
                $phoneExist->where('telephone', $request->selected_telephone)->update(['is_primary' => '1']);
                $phoneExist->save();
            } else {
                $contact->phone()->create(['telephone' => $newPhone]);
            }
            $contact->phone()->where('telephone', $request->selected_telephone)->update(['is_primary' => '1']);
        }

        $removedEmails = $contact->email->whereNotIn('email', $emails);
        foreach ($removedEmails as $removedEmail) {
            $removedEmail->delete();
        }

        $removedPhones = $contact->phone->whereNotIn('telephone', $phones);
        foreach ($removedPhones as $removedPhone) {
            $removedPhone->delete();
        }

        $contact->update($data);

        return redirect(route('contacts.index'))
            ->with([
                'status' => 'success',
                'message' => 'Contact Updated Successfully!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        $contact->deleteImage();
        return redirect(route('contacts.index'))->with(
            [
                'status' => 'success',
                'message' => 'Contact Deleted Successfully!'
            ]
        );
    }
}
