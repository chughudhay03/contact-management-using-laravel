<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'birthdate',
        'address',
        'image_name'
    ];

    public function email()
    {
        return $this->hasMany(Email::class);
    }

    public function phone()
    {
        return $this->hasMany(Phone::class);
    }

    public function deleteImage() {
        Storage::delete($this->image_name);
    }

    public function scopeSearch($query)
    {
        $search = request('search');
        if($search)
        {
            return $query->where('first_name', 'like', "%$search%")
                    ->orWhere('last_name', 'like', "%$search%")
                    ->orWhereHas('email', function ($emailQuery) use ($search) {
                        $emailQuery->where('email', 'like', "%$search%");
                    })
                    ->orWhereHas('phone', function ($phoneQuery) use ($search) {
                        $phoneQuery->where('telephone', 'like', "%$search%");
                    })
                    ->orWhere('birthdate', 'like', "%$search%")
                    ->orWhere('address', 'like', "%$search%");
        }
        return $query;
    }
}
