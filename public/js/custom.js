$(function() {
    $('.modal').modal();

    $(".dropdown-trigger").dropdown();

    $(".close-alert").click(function() {
        $(this).parent().hide('fade-out');
    });
});

$(document).ready(function(){
    var maxField = 1;
    var search = $('.search');
    var wrapper = $('.wrapper');
    var fieldHTML = '<div class="row mb-0"><form action=""><div class="form-group"><input type="search" name="search" class="form-control mx-2" placeholder="Search Contacts"/><button class="btn btn-primary mx-2" type="submit">Search</button><button class="btn btn-secondary remove" type="button">Close</button></div></form></div>';
    var textField = 1;

    $(search).click(function(){
        if(textField == maxField){
            $(wrapper).append(fieldHTML);
            maxField++;
        }
    });

    $(wrapper).on('click', '.remove', function(e){
        e.preventDefault();
        $(this).parent('div').remove();
        textField++;
    });
});
