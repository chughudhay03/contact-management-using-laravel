const inputMails = document.getElementsByName('emails[]');
const addBtnMail = document.getElementById('add_email');
const mailWrapper = document.querySelector('.mailwrapper');
const inputTeles = document.getElementsByName('telephones[]');
const addBtnTele = document.getElementById('add_phone');
const teleWrapper = document.querySelector('.telewrapper');
const radioEmailBtns = document.getElementsByName('selected_email');
const radioTeleBtns = document.getElementsByName('selected_telephone');

loadEvents();

function loadEvents() {
    document.addEventListener('DOMContentLoaded',getDefaultRadioBtn);
    document.addEventListener('DOMContentLoaded',getOldValue);
    for (var i = 0; i < inputMails.length; i++) {
        inputMails[i].addEventListener('keyup', displayMailBtn);
    }
    addBtnMail.addEventListener('click', dynamicMailField);
    for (var i = 0; i < inputTeles.length; i++) {
        inputTeles[i].addEventListener('keyup', displayPhoneBtn);
    }
    addBtnTele.addEventListener('click', dynamicPhoneField);
    for (var i = 0; i < radioEmailBtns.length; i++) {
        radioEmailBtns[i].addEventListener('change', getRadioValue);
    }
    for (var i = 0; i < radioTeleBtns.length; i++) {
        radioTeleBtns[i].addEventListener('change', getRadioValue);
    }
}

function displayMailBtn(event) {
    const val = event.target.value;
    const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const result = val.match(pattern);
    if(result)
        show(addBtnMail);
    else
        hide(addBtnMail);
}

function displayPhoneBtn(event) {
    const val = event.target.value;
    const pattern = /^\d{10}$/;
    const result = val.match(pattern);
    if(result)
        show(addBtnTele);
    else
        hide(addBtnTele);
}

function dynamicMailField(value) {
    let mainDiv = document.createElement('div');
    let radioDiv = document.createElement('div');
    let radioLabel = document.createElement('label');
    let radioInput = document.createElement('input');
    let radioSpan = document.createElement('span');
    let inputDiv = document.createElement('div');
    let createInput = document.createElement('input');
    let buttonDiv = document.createElement('div');
    let createButton = document.createElement('button');

    mainDiv.className = 'row mb10 email';

    radioDiv.className = 'input-field col s05';
    radioInput.id = 'radio';
    radioInput.className = 'with-gap';
    radioInput.name = 'selected_email';
    radioInput.type = 'radio';
    if (value === event) {
        radioInput.value = '';
    } else {
        radioInput.value = value;
    }
    radioInput.addEventListener('change', getRadioValue);

    inputDiv.className = 'input-field col s40';

    createInput.className = 'validate';
    createInput.id = 'email';
    createInput.type = 'email';
    createInput.name = 'emails[]';
    if (value === event) {
        createInput.value = '';
    } else {
        createInput.value = value;
    }
    OnInputEmpty(addBtnMail);
    createInput.addEventListener('keyup', displayMailBtn);

    buttonDiv.className = 'input-field col s05';

    createButton.className = 'btn-floating btn-medium waves-effect waves-light red';
    createButton.id = 'remove_email';
    createButton.name = 'remove_email';
    createButton.type = 'button';
    createButton.innerHTML = '<i class="material-icons">remove</i>';
    createButton.addEventListener('click',removeField);

    radioLabel.appendChild(radioInput);
    radioLabel.appendChild(radioSpan);

    radioDiv.appendChild(radioLabel);
    inputDiv.appendChild(createInput);
    buttonDiv.appendChild(createButton);

    mainDiv.appendChild(radioDiv);
    mainDiv.appendChild(inputDiv);
    mainDiv.appendChild(buttonDiv);

    mailWrapper.appendChild(mainDiv);
}

function dynamicPhoneField(value) {
    let mainDiv = document.createElement('div');
    let radioDiv = document.createElement('div');
    let radioLabel = document.createElement('label');
    let radioInput = document.createElement('input');
    let radioSpan = document.createElement('span');
    let inputDiv = document.createElement('div');
    let createInput = document.createElement('input');
    let buttonDiv = document.createElement('div');
    let createButton = document.createElement('button');

    mainDiv.className = 'row mb10 phone';

    radioDiv.className = 'input-field col s05';
    radioInput.id = 'radio';
    radioInput.className = 'with-gap';
    radioInput.name = 'selected_telephone';
    radioInput.type = 'radio';
    if (value === event) {
        radioInput.value = '';
    } else {
        radioInput.value = value;
    }
    radioInput.addEventListener('change', getRadioValue);

    inputDiv.className = 'input-field col s90';

    createInput.className = 'validate';
    createInput.id = 'telephone'
    createInput.type = 'tel';
    createInput.name = 'telephones[]';
    if (value === event) {
        createInput.value = '';
    } else {
        createInput.value = value;
    }
    OnInputEmpty(addBtnTele);
    createInput.addEventListener('keyup', displayPhoneBtn);

    buttonDiv.className = 'input-field col s05';

    createButton.className = 'btn-floating btn-medium waves-effect waves-light red';
    createButton.id = 'remove_phone';
    createButton.type = 'button';
    createButton.innerHTML = '<i class="material-icons">remove</i>';
    createButton.addEventListener('click',removeField);

    radioLabel.appendChild(radioInput);
    radioLabel.appendChild(radioSpan);

    radioDiv.appendChild(radioLabel);
    inputDiv.appendChild(createInput);
    buttonDiv.appendChild(createButton);

    mainDiv.appendChild(radioDiv);
    mainDiv.appendChild(inputDiv);
    mainDiv.appendChild(buttonDiv);

    teleWrapper.appendChild(mainDiv);
}

function removeField() {
    var parentParentElement = this.parentElement.parentElement;
    parentParentElement.remove();

    if(parentParentElement.className.includes('phone'))
        show(addBtnTele);
    else if(parentParentElement.className.includes('email'))
        show(addBtnMail);
}

function getRadioValue() {
    for (var i = 0; i < radioEmailBtns.length; i++) {
        if (radioEmailBtns[i].checked) {
            radioEmailBtns[i].value = inputMails[i].value;
            console.log(radioEmailBtns[i]);
        }
    }
    for (var i = 0; i < radioTeleBtns.length; i++) {
        if (radioTeleBtns[i].checked) {
            radioTeleBtns[i].value = inputTeles[i].value;
            console.log(radioTeleBtns[i]);
        }
    }
}

function getDefaultRadioBtn() {
    for (let i = 0; i < radioEmailBtns.length; i++) {
        if (radioEmailBtns[i].checked) {
          radioEmailBtns[i].value = inputMails[i].value;
          inputMails[i].addEventListener('change', function(event) {
            radioEmailBtns[i].value = event.target.value;
            console.log(radioEmailBtns[i]);
          });
        }
    }
    for (let i = 0; i < radioTeleBtns.length; i++) {
        if (radioTeleBtns[i].checked) {
          radioTeleBtns[i].value = inputTeles[i].value;
          inputTeles[i].addEventListener('change', function(event) {
            radioTeleBtns[i].value = event.target.value;
            console.log(radioTeleBtns[i]);
          });
        }
    }
}

function getOldValue() {
    for (let i = 0; i < inputMails.length; i++) {
        let data = inputMails[i].value;
        let emails = data.split(",");
        console.log(emails);
        if(inputMails[i].value.includes(':'+1))
            radioEmailBtns[i].checked = 'true';
        inputMails[i].value = emails[0].replace(/[\[\]\{\}"]+/g,'').split(':')[0];
        radioEmailBtns[i].value = emails[0].replace(/[\[\]\{\}"]+/g,'').split(':')[0];
        console.log(emails[0]);
        for (let j = 1; j < emails.length; j++) {
            console.log(emails[j]);
            dynamicMailField(emails[j]);
        }
    }
    for (let i = 0; i < inputTeles.length; i++) {
        let data = inputTeles[i].value;
        let phones = data.split(",");
        console.log(phones);
        if(inputTeles[i].value.includes(':'+1))
            radioTeleBtns[i].checked = 'true';
        inputTeles[i].value = phones[0].replace(/[\[\]\{\}"]+/g,'').split(':')[0];
        radioTeleBtns[i].value = phones[0].replace(/[\[\]\{\}"]+/g,'').split(':')[0];
        console.log(phones[0]);
        for (let j = 1; j < phones.length; j++) {
            console.log(phones[j]);
            dynamicPhoneField(phones[j]);
        }
    }
}

function OnInputEmpty(element) {
    if(displayMailBtn||displayPhoneBtn)
        hide(element);
    else
        show(element);
}

function show(element) {
    element.style.display = 'block';
}

function hide(element) {
    element.style.display = 'none';
}
