@extends('contacts.layouts.app')

@section('title', 'Contact Management - By Udhay Chugh')

@section('main-content')
    <a href="{{ route('contacts.create') }}" class="btn btn-primary px-3 py-0 mx-2" type="submit">Add Contact <i class="fa fa-plus"></i></a>
    <a href="#" class="btn btn-outline-primary px-3 py-0 search" type="submit"><i class="fa fa-filter"></i> Filter</a>
    <div class="wrapper mt-3 mb-0"></div>
    <div class="d-grid m-1 cards">
        @foreach ($contacts as $contact)
            <div class="card m-1 cardSize position-relative">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="left">
                            <h5 class="card-text fw-bold">Name: {{ $contact->first_name." ".$contact->last_name }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Email: {{ $contact->email->where('is_primary', 1)->pluck('email')->first() }}</h6>
                            <p class="card-text mb-2">Phone: {{ $contact->phone->where('is_primary', 1)->pluck('telephone')->first() }}</p>
                            <h6 class="card-subtitle mb-2">DOB: {{ $contact->birthdate }}</h6>
                            <p class="card-text text-muted m-0">Address: {{ $contact->address }}</p>
                        </div>
                        <div class="right m-auto mt-0">
                            <img src="{{ asset("storage/$contact->image_name") }}" class="circle" alt="..." height="60%" >
                        </div>
                    </div>
                    <div class="pt-4">
                        <a href={{ route('contacts.edit', $contact) }} class="btn btn-outline-secondary px-3 py-0" type="submit">Edit <i class="fa fa-pencil-square-o"></i></a>
                        <button href="#deleteModal" class="btn btn-outline-danger px-3 py-0 modal-trigger delete" data-bs-toggle="modal" data-bs-target="#deleteModal" data-contact-id="{{ $contact->id }}" type="submit" >Delete <i class="fa fa-trash-o"></i></button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    {{ $contacts->appends(['search'=>request('search')])->links('contacts.layouts.partials._pagination') }}
@endsection

@section('modal')
    <div id="deleteModal" class="modal">
        <div class="modal-content">
            <h4>Delete Contact</h4>
            <p>Are you sure you want to delete the contact?</p>
        </div>
        <div class="modal-footer">
            <form id="deleteForm" method="POST">
                @csrf
                @method('DELETE')
                <a type="button" class="modal-close btn btn-secondary waves-effect">Cancel</a>
                <button type="submit" class="modal-close btn btn-danger waves-effect">Yes, Delete</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        document.querySelectorAll('.delete').forEach(function(btn){
            btn.addEventListener('click',handleDeleteClick);
        });

        function handleDeleteClick(evt){
            console.log(evt.target.dataset.contactId);
            const contactId = evt.target.dataset.contactId;
            const URL = `/contacts/${contactId}`;
            console.log(URL);
            document.getElementById('deleteForm').setAttribute('action',URL)
        }
    </script>
@endsection
