@extends('contacts.layouts.app')

@section('title', 'Edit Contact - By Udhay Chugh')

@section('main-content')
    <div class="container">
        <div class="row mt50 text-primary">
            <h2>Edit Old Contact</h2>
        </div>
        <div class="row">
            <form class="col s12 formValidate" action="{{ route("contacts.update", $contact) }}" id="update-contact-form" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row mb10">
                    <div class="input-field col s6">
                        <input id="first_name" name="first_name" type="text" class="validate" data-error=".first_name_error" value="{{ $contact->first_name }}">
                        <label for="first_name">First Name</label>
                        <div class="first_name_error "></div>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" name="last_name" type="text" class="validate" data-error=".last_name_error" value="{{ $contact->last_name }}">
                        <label for="last_name">Last Name</label>
                        <div class="last_name_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s05">
                        <label><input id="radio" class="with-gap" name="selected_email" type="radio"><span></span></label>
                    </div>
                    <div class="input-field col s40">
                        <input id="email" name="emails[]" type="email" class="validate" data-error=".email_error" value="{{ $contact->email->pluck('is_primary', 'email') }}">
                        <label for="email">Email</label>
                        <div class="email_error "></div>
                    </div>
                    <div class="input-field col s05">
                        <button id="add_email" class="btn-floating btn-medium waves-effect waves-light blue" type="button"><i class="material-icons">add</i></button>
                    </div>
                    <div class="input-field col s6">
                        <input id="birthdate" name="birthdate" type="text" class="datepicker" data-error=".birthday_error" value="{{ $contact->birthdate }}">
                        <label for="birthdate">Birthdate</label>
                        <div class="birthday_error "></div>
                    </div>
                </div>
                <div class="mailwrapper"></div>
                <div class="row mb10">
                    <div class="input-field col s05">
                        <label><input id="radio" class="with-gap" name="selected_telephone" type="radio"/><span></span></label>
                    </div>
                    <div class="input-field col s90">
                        <input id="telephone" name="telephones[]" type="tel" class="validate" data-error=".telephone_error" value="{{ $contact->phone->pluck('is_primary', 'telephone') }}">
                        <label for="telephone">Telephone</label>
                        <div class="telephone_error "></div>
                    </div>
                    <div class="input-field col s05">
                        <button id="add_phone" class="btn-floating btn-medium waves-effect waves-light blue" type="button"><i class="material-icons">add</i></button>
                    </div>
                </div>
                <div class="telewrapper"></div>
                <div class="row mb10">
                    <div class="input-field col s12">
                        <textarea id="address" name="address" class="materialize-textarea" data-error=".address_error">{{ $contact->address }}</textarea>
                        <label for="address">Address</label>
                        <div class="address_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="file-field input-field col s12">
                        <div class="col s3">
                            <img class="circle " id="temp_image" src="{{ asset("storage/$contact->image_name") }}" >
                        </div>
                        <div class="btn btn-primary">
                            <span>Image</span>
                            <input type="file" name="image_name" id="pic" data-error=".pic_error">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Upload Your Image" value="{{ trim($contact->image_name, "contacts/");}}">
                        </div>
                        <div class="pic_error "></div>
                    </div>
                </div>
                <button class="btn btn-primary waves-effect waves-light right px-3 py-1" type="submit" name="action">Update
                    <i class="material-icons right">send</i>
                </button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src={{ asset("js/vendors/jquery-validation/validation.min.js") }} type="text/javascript"></script>
    <script src={{ asset("js/vendors/jquery-validation/additional-methods.min.js") }} type="text/javascript"></script>
    <script src={{ asset("js/pages/update-contact.js") }}></script>
    <script src={{ asset("js/pages/home.js") }}
@endsection
