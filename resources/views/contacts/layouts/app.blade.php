<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href={{ asset("css/materialize.min.css") }} type="text/css">
    <link rel="stylesheet" href={{ asset("css/bootstrap.min.css") }} type="text/css">
    <link rel="stylesheet" href={{ asset("css/style.css") }} type="text/css">
    @yield('styles')
    <title>@yield('title')</title>
</head>
<body class="blue darken-1">
    <div class="w-100 h-100 py-3 px-3">
        <div class="card">
            <div class="card-header text-center text-primary fw-bold fs-2 font-monospace p-0">
              Contact Management
            </div>
            <div class="card-body blue lighten-4">
                @include('contacts.layouts.partials._message')
                @yield('main-content')
            </div>
            @yield('modal')
            @include('contacts.layouts.partials._footer')
        </div>
    </div>
    <script src={{ asset("js/jquery.min.js") }} type="text/javascript"></script>
    <script src={{ asset("js/materialize.min.js") }} type="text/javascript"></script>
    <script src={{ asset("js/custom.js") }} type="text/javascript"></script>
    @yield('scripts')
</body>
</html>
