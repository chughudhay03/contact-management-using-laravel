<footer class="sticky-footer grey lighten-5">
    <div class="container my-auto">
        <div class="copyright text-center my-auto text-primary">
            <span>Copyright &copy; Contact Management 2023</span>
        </div>
    </div>
</footer>
