@if(session()->has('status'))
<div class="materialert {{ session()->get('status') }} fw-bold fs-5 font-monospace" role="alert">
    <div class="material-icons">check</div>
    <span>{{ session()->get('message') }}</span>
    <button type="button" class="close-alert">×</button>
</div>
@endif
